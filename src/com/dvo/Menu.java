package com.dvo;

import com.sun.org.apache.xpath.internal.SourceTree;

import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by User on 27.08.2017.
 */
public class Menu {
    private final String urlString = "jdbc:mysql://127.0.0.1:3306/auction";
    private ResultSet resultSet;
    Connection connection;
    Statement statement;
    PreparedStatement prepState;
    Scanner scanner = new Scanner(System.in);

    public Menu() {
        try {
            connection = DriverManager.getConnection(urlString, "root", "root");
            statement = connection.createStatement();
        } catch (Exception ex) {
            System.out.println("Error while connect to DB!");
        }
        System.out.println("Welcome to the auction!\nPlease enter your action!");
        showInitialMenu();
    }

    private void showInitialMenu() {
        System.out.println("Main menu:");
        System.out.println("[1] - Start auction");
        System.out.println("[2] - Add seller");
        System.out.println("[3] - Add product");
        System.out.println("[4] - Add buyer");
        System.out.println("[5] - Add bid from buyer");
        System.out.println("[6] - Exit");
        int level = tryInput();
        switch (level) {
            case 1:
                showStartAuctionMenu();
                break;
            case 2:
                try {
                    showAddSellerMenu();
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                break;
            case 3:
                try {
                    showAddProductMenu();
                } catch (Exception ex) {
                    System.out.println(ex);
                }
                break;
            case 4:
                try {
                    showAddBuyerMenu();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case 5:
                try {
                    showAddBidMenu();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case 6:
                System.out.println("Auction is closed");
                break;
            default:
                System.out.println("Index out of range! You will be returned to main menu");
                showInitialMenu();
                break;
        }
    }

    private void showAddBidMenu() throws SQLException {
        System.out.println("Add Buyer:");
        System.out.println("[1] - Start adding a Bid");
        System.out.println("[2] - Return to main menu");

        int level = tryInput();

        switch (level) {
            case 1:
                addBid();
                break;
            case 2:
                showInitialMenu();
                break;
            default:
                System.out.println("Index out of range! You will be returned to main menu");
                showInitialMenu();
        }
    }

    private void addBid() throws SQLException {
        Integer bid_step;
        Integer current_bid;
        Integer buyer_id;
        Integer product_id;

        String prepBid = "INSERT into bid (bid_step, current_bid, buyer_id, product_id)" +
                "values (?, ?, ?, ?)";

        System.out.println("Adding Bid:");
        System.out.println("Please, enter bid step:");
        bid_step = tryInput();
        System.out.println("Please, enter current bid:");
        current_bid = tryInput();
        System.out.println("Please, select buyer id from list: ");
        showAllEntries("buyer", false);
        buyer_id = tryInput();
        System.out.println("Please, select product id from list: ");
        showAllEntries("product", false);
        product_id = tryInput();
        prepState = connection.prepareStatement(prepBid);
        prepState.setInt(1,bid_step);
        prepState.setInt(2,current_bid);
        prepState.setInt(3,buyer_id);
        prepState.setInt(4, product_id);
        prepState.execute();
        System.out.println("Bid was placed. Now you will be moved to main menu");
        showInitialMenu();
    }

    private void showAddBuyerMenu() throws SQLException {
        System.out.println("Add Buyer:");
        System.out.println("[1] - Start adding a Buyer");
        System.out.println("[2] - Show all Buyers");
        System.out.println("[3] - Return to main menu");

        int level = tryInput();

        switch (level) {
            case 1:
                addBuyer();
                break;
            case 2:
                showAllEntries("buyer", true);
                showAddBuyerMenu();
                break;
            case 3:
                showInitialMenu();
                break;
            default:
                System.out.println("Index out of range! You will be returned to main menu");
                showInitialMenu();
        }
    }

    private void addBuyer() throws SQLException {
        String buyer_name;
        String buyer_phone;
        String buyer_address;
        String buyer_auction_number;

        String prepSeller =
                "INSERT into buyer (buyer_name, buyer_phone, buyer_address, buyer_auction_number)" +
                        " values (?, ?, ?, ?)";
        System.out.println("Adding Buyer:");
        System.out.println("Please, enter Buyer's name:");
        buyer_name = scanner.nextLine();
        System.out.println("Please, enter Buyer's phone:");
        buyer_phone = scanner.nextLine();
        System.out.println("Please, enter Buyer's address:");
        buyer_address = scanner.nextLine();
        System.out.println("Please, enter Buyer's auction number:");
        buyer_auction_number = scanner.nextLine();
        prepState = connection.prepareStatement(prepSeller);
        prepState.setString(1, buyer_name);
        prepState.setString(2, buyer_phone);
        prepState.setString(3, buyer_address);
        prepState.setString(4, buyer_auction_number);
        prepState.execute();
        System.out.println("Buyer was added!\nNow you will be returned to main menu\n\n");
        showInitialMenu();
    }

    private void showStartAuctionMenu() {
        System.out.println("Start auction:");
        System.out.println("[1] - Start auction for product");
        System.out.println("[2] - Show all products");
        System.out.println("[3] - Return to main menu");
        int level = tryInput();

        switch (level) {
            case 1:
                break;
            case 2:
                break;
            case 3:
                showInitialMenu();
                break;
            default:
                System.out.println("Index out of range! You will be returned to main menu");
                showInitialMenu();
                break;
        }
    }

    private void showAddSellerMenu() throws SQLException {
        System.out.println("Add Seller:");
        System.out.println("[1] - Start adding a seller");
        System.out.println("[2] - Show all sellers");
        System.out.println("[3] - Return to main menu");

        int level = tryInput();

        switch (level) {
            case 1:
                addSeller();
                break;
            case 2:
                showAllEntries("seller", true);
                showAddSellerMenu();
                break;
            case 3:
                showInitialMenu();
                break;
            default:
                System.out.println("Index out of range! You will be returned to main menu");
                showInitialMenu();
        }
    }

    private void addSeller() throws SQLException {
        String seller_name;
        String seller_address;
        String seller_phone;
        String seller_license_number;

        String prepSeller =
                "INSERT into seller (seller_name, seller_address, seller_phone, seller_license_number)" +
                        " values (?, ?, ?, ?)";
        System.out.println("Adding Seller:");
        System.out.println("Please, enter Seller's name:");
        seller_name = scanner.nextLine();
        System.out.println("Please, enter Seller's address:");
        seller_address = scanner.nextLine();
        System.out.println("Please, enter Seller's phone:");
        seller_phone = scanner.nextLine();
        System.out.println("Please, enter Seller's license number:");
        seller_license_number = scanner.nextLine();
        System.out.println("Seller was added!\nNow you will be returned to main menu\n\n");
        prepState = connection.prepareStatement(prepSeller);
        prepState.setString(1, seller_name);
        prepState.setString(2, seller_address);
        prepState.setString(3, seller_phone);
        prepState.setString(4, seller_license_number);
        prepState.execute();
        showInitialMenu();

    }

    private void showAddProductMenu() throws SQLException {
        System.out.println("Add product menu:");
        System.out.println("[1] - Start adding product");
        System.out.println("[2] - Show all products");
        System.out.println("[3] - Return to main menu");

        int level = tryInput();

        switch (level) {
            case 1:
                addProduct();
                break;
            case 2:
                showAllEntries("product", true);
                showAddProductMenu();
                break;
            case 3:
                showInitialMenu();
                break;
            default:
                System.out.println("Index out of range! You will be returned to main menu");
                showInitialMenu();

        }
    }


    private void addProduct() throws SQLException {
        String product_name;
        Integer product_start_price;
        String product_category;
        Integer seller_id;
        String preparedString = "INSERT into product (product_name, product_start_price, product_category, seller_id) " +
                "values (?, ?, ?, ?)";
        prepState = connection.prepareStatement(preparedString);
        System.out.println("Adding new product\n");
        System.out.println("Enter product name: ");
        product_name = scanner.nextLine();
        System.out.println("Enter product start price: ");
        product_start_price = tryInput();
        System.out.println("Enter product category: ");
        product_category = scanner.nextLine();
        System.out.println("Enter product's seller id from list: \n");
        showAllEntries("seller", false);
        seller_id = tryInput();
        prepState.setString(1,product_name);
        prepState.setInt(2,product_start_price);
        prepState.setString(3,product_category);
        prepState.setInt(4,seller_id);
        prepState.execute();
        System.out.println("Product was added. You will be moved to main menu");
        showInitialMenu();

    }

    private void showAllEntries(String tableName, boolean fullTable) throws SQLException {
        PreparedStatement prepState;
        String preparedString = "select * from " + tableName;
        prepState = connection.prepareStatement(preparedString);
        resultSet = prepState.executeQuery();
        ResultSetMetaData rsmd = resultSet.getMetaData();
        int columnCount = rsmd.getColumnCount();
        if (!fullTable) {
            columnCount = 2;
            System.out.println("\nAll entries from " + tableName + " (shorted)\n");
        } else {
            System.out.println("\nAll entries from " + tableName + " \n");
        }

        while (resultSet.next()) {
            for (int i = 1; i <= columnCount; i++) {
                System.out.print(resultSet.getObject(i) + "  ");
            }
            System.out.println();
        }
        System.out.println("\n");

    }

    private int tryInput() {
        String level = "";
        try {
            level = scanner.nextLine();
        } catch (InputMismatchException exc) {
            scanner.next();
            System.out.println("Wrong selection, you will be returned to the main menu");
            showInitialMenu();
        }
        return Integer.parseInt(level);
    }
}
